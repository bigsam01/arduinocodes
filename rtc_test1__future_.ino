#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 rtc;
String fri = "Friday";
String sat = "Saturday";
String sun = "Sunday";
String currentDay;

 // create an RTCInt type object
  //int type to check the hour.
void setup() {
  pinMode(13, OUTPUT);
  // put your setup code here, to run once:
  while (!Serial); // for Leonardo/Micro/Zero

  Serial.begin(9600);
  
  //Set RTC to current day the code was compiled.
  rtc.begin();
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
  }
}
  int checkHour = 8;
  
  int friBreak(int hr, int mn, int sc){
    if(hr == 11 && mn == 30 && sc == 0){
      return 1;
    }
    else if(hr == 12 && mn == 0 && sc == 0){
      return 1;
    }
    else{
      return 0;
    }
    //Break time on fridays.
  }
  int workDayBreak(int hr, int mn, int sc){
    if(hr == 10 && mn == 30 && sc == 0){
      return 1;
    }
    else if(hr == 10 && mn == 40 && sc == 0){
      return 1;
    }
    
    else if(hr == 12 && mn == 0 && sc == 0){
      return 1;
    }
    else if(hr == 12 && mn == 30 && sc == 0){
      return 1;
    }
    else if(hr == 14 && mn == 0 && sc == 0){
      return 1;
    }
    else if(hr == 14 && mn == 30 && sc == 0){
      return 1;
    }
    else{
      return 0;
    }
    
    //Break time on other work days asides friday.
  }
  //Function for getting the day of the week.
  int dow(int y, int m, int d){
      static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
      y -= m < 3;
      return (y + y/4 - y/100 + t[m - 1] + d) % 7;
    }
    
void loop() {
  
  // put your main code here, to run repeatedly:
  DateTime nows = rtc.now();
  
  int months = nows.month();
  int hours = nows.hour();
  int mins = nows.minute();
  int sec = nows.second();
  int days = nows.day();
  int years = nows.year();
  
  
  //check for the day we are currently in.
  int currentDow = dow(years, months, days);
  switch(currentDow){
    case 0:
      currentDay = "Sunday";
      break;
    case 1:
      currentDay = "Monday";
      break;
    case 2:
      currentDay = "Tuesday";
      break;  
    case 3:
      currentDay = "Wednessday";
      break;
    case 4:
      currentDay = "Thursday";
      break;
    case 5:
      currentDay = "Friday";
      break;
    case 6:
      currentDay = "Saturday";
      break;
    default:
      break;
  }
  if(hours < 8 || hours > 16){
      digitalWrite(13, LOW);
    }
    int frid = friBreak(hours, mins, sec);
    if(frid == 1){
      for(int i = 0; i <= 8; i++){
        digitalWrite(13, HIGH);
        delay(2000);
        digitalWrite(13, LOW);
        delay(1000);
      }
    }
    //Check for break time
    //Friday starts on counting by 8am and ends 1pm.
    DateTime future (nows + TimeSpan(1,0,0,0));
    //Time setting for friday is different.
    if(fri == currentDay){
      //Check for the break time every friday.
      
      if (future.hour() == nows.hour()){
        if (checkHour <= 1){
           digitalWrite(13, HIGH);
           delay(30000);
           checkHour++;
        }
        //Have to write code for break-time periods.
      }
    }
    
    //It's a week day other than friday.
    else{
      //Check if it's weekend days.
      if (sat == currentDay || sun == currentDay){
        digitalWrite(13, LOW);
      }
      else{
        //It has to be a working day excluding friday.
        //Same goes for this, have to write codes for break-time periods.
        //First of all check for the breaks of the day.
        int workDayBk = workDayBreak(hours, mins, sec);
        if(workDayBk == 1){
          for(int i = 0; i <= 8; i++){
            digitalWrite(13, HIGH);
            delay(2000);
            digitalWrite(13, LOW);
            delay(1000);
      }
        }
        if (future.hour() == nows.hour()){
            if (checkHour <= 16) {
            digitalWrite(13, HIGH);
            delay(30000);
            checkHour++;
            }
          }
      }
    }
    delay(3000);
}
